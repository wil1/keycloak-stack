#!/bin/bash

# check if secrets exist
checkSecret() {
    SECRET=$1
    [ ! "$(docker secret ls | grep ${SECRET})" ] \
    && echo "
    Secret required: 
        ${SECRET}
    To create the secret:
        echo \"MySecret\" | docker secret create ${SECRET} -
    " \
    && exit 1
    
}
checkSecret keycloak_admin_password
checkSecret keycloak_db_password
    
# create network
[ ! "$(docker network ls | grep 'nginx_proxy')" ] \
    && docker network create -d overlay nginx_proxy

# merge docker-compose.yml and .env file (docker-compose config), and deploy it to docker swarm as a stack
# (docker-compose config) - https://github.com/moby/moby/issues/29133#issuecomment-442912378
docker stack deploy -c <(docker-compose config) keycloak
